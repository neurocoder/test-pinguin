﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PinguinsStore  {
    private static List<GameObject> pinguins = new List<GameObject>();
    public static Action finishAct;
    public static int CountPings()
    {
        return pinguins.Count;
    }

    public static void AddPinguin(GameObject ping)
    {
        pinguins.Add(ping);
    }

    public static void RemovePinguin(GameObject ping)
    {
        pinguins.Remove(ping);   
        if(pinguins.Count == 0)
        {
            if (finishAct != null)
            {
                finishAct();
            }
        }
    }

}
