﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpawn : MonoBehaviour {
    public GameObject playerGO;


    public float spawnDelay = 1.5f;
	// Use this for initialization
	void Start () {
        StartCoroutine(SpawnPlayer());
	}
	
	IEnumerator SpawnPlayer()
    {
        yield return new WaitForSecondsRealtime(spawnDelay);       
        playerGO.SetActive(true);
    }
}
