﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PinguinsSpawn : MonoBehaviour {
    
    public int countSpawnPinguins = 15;
    public GameObject pinguinGO;
    public Transform spawnPoint;


    private void Start()
    {
        StartCoroutine(SpawnWithDelay());
    }

    // Update is called once per frame
    void Update () {
       
	}

    IEnumerator SpawnWithDelay()
    {
        for (int i = 0; i < countSpawnPinguins; i++)
        {
            GameObject newPing = Instantiate(pinguinGO);
            PinguinsStore.AddPinguin(newPing);
            yield return new WaitForSecondsRealtime(0.1f);
        }
    }
}
