﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UICounter : MonoBehaviour {

    public Text txtCounter;
    
	// Update is called once per frame
	void Update ()
    {
        txtCounter.text = "pinguins left: " + PinguinsStore.CountPings();
    }
}
