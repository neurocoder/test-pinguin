﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinishWindowCtrl : MonoBehaviour {

    public void Restart()
    {
        Application.LoadLevel(Application.loadedLevel);
    }

    public void Quit()
    {
        Application.Quit();
    }
}
