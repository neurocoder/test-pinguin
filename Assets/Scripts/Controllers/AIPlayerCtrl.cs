﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIPlayerCtrl : MonoBehaviour
{	
    public float speed = 6.0f;
    public float gravity = 20.0f;



    private CharacterController controller;
    private float timer = 0;
    private Vector3 moveDirection = Vector3.zero;
    private Vector3 randomDir;

    void Start()
    {
        controller = GetComponent<CharacterController>();
        gameObject.transform.position = new Vector3(-3.5f, 5, 0);
        randomDir = RandomizeDir();
    }

    void Update()
    {
        timer -= Time.deltaTime;
        if (controller.isGrounded)
        {
            if (timer < 0)
            {
                timer = 2f;
                randomDir = RandomizeDir();
            }
            moveDirection = randomDir;
            moveDirection = transform.TransformDirection(moveDirection);
            moveDirection = moveDirection * speed;
        }

        moveDirection.y = moveDirection.y - (gravity * Time.deltaTime);
        controller.Move(moveDirection * Time.deltaTime);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "grid" || other.tag == "finish")
        {
            randomDir = (transform.position - other.ClosestPoint(transform.position)).normalized;            
            timer = 2f;
        }
        if(other.tag == "player")
        {
            randomDir = RandomizeDir();
        }
    }

   
    private Vector3 RandomizeDir()
    {
        return new Vector3(Random.Range(-1f, 1f), 0, Random.Range(-1f, 1f));
    }
}
