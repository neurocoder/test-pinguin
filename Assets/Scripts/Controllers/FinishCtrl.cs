﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class FinishCtrl : MonoBehaviour {

    public Transform finishPoint;
    public GameObject finishWindow;

    private void Start()
    {
        PinguinsStore.finishAct += ShowFinishWindow;
    }

    private void OnTriggerEnter(Collider collider)
    {
        if(collider.tag == "pinguin")
        {
            collider.transform.GetComponent<PinguinCtrl>().enabled = false;
            PinguinsStore.RemovePinguin(collider.gameObject);
            StartCoroutine(Finish(collider.transform));
        }
    }


    private IEnumerator Finish(Transform pinguin)
    {
        if (transform != null)
        {
            var direction = (finishPoint.position - pinguin.position).normalized;
            while (Vector3.Distance(pinguin.position, finishPoint.position) > 0.1f)
            {
                pinguin.transform.position += direction * 0.05f;
                yield return null;
            }
            DestroyImmediate(pinguin.gameObject);
        }
      
    }

    private void ShowFinishWindow()
    {
        finishWindow.SetActive(true);
    }
}
