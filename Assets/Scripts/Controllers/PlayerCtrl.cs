﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(CharacterController))]
public class PlayerCtrl : MonoBehaviour
{
    public float speed = 6.0f;
    public float gravity = 20.0f;

    public Transform eyePivot;

    private Vector3 moveDirection = Vector3.zero;
    private CharacterController controller;


    void Start()
    {
        controller = GetComponent<CharacterController>();
        gameObject.transform.position = new Vector3(0, 5, 0);
        
    }

    void Update()
    {
        //controll eye (fake rotation)
        if (Input.GetKeyDown(KeyCode.LeftArrow))
            eyePivot.transform.eulerAngles = new Vector3(0, -90f, 0);
        if (Input.GetKeyDown(KeyCode.RightArrow))
            eyePivot.transform.eulerAngles = new Vector3(0, 90f, 0);
        if (Input.GetKeyDown(KeyCode.UpArrow))
            eyePivot.transform.eulerAngles = new Vector3(0, 0, 0);
        if (Input.GetKeyDown(KeyCode.DownArrow))
            eyePivot.transform.eulerAngles = new Vector3(0, 180f, 0);

        if (controller.isGrounded)
        {         
            moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0.0f, Input.GetAxis("Vertical"));
            moveDirection = transform.TransformDirection(moveDirection);
            moveDirection = moveDirection * speed;          
        }
             
        moveDirection.y = moveDirection.y - (gravity * Time.deltaTime);
        controller.Move(moveDirection * Time.deltaTime);
    }

}
