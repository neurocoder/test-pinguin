﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


[RequireComponent(typeof(CharacterController))]
public class PinguinCtrl : MonoBehaviour
{

    public float speed = 6.5f;
    public float gravity = 20.0f;


    private float timer = 0;
    private Vector3 moveDirection = Vector3.zero;
    private Vector3 randomDir;
    private Vector3 randomNewDir;
    private CharacterController controller;

    private float newAngle = 30f;
    private float steps = 3f;



    void Start()
    {
        controller = GetComponent<CharacterController>();
        gameObject.transform.position = new Vector3(Random.RandomRange(-3f,7f), 5, Random.RandomRange(-8f, 8f));
        randomDir = RandomizeDir();
        randomNewDir = RandomizeDir();
    }



    void Update()
    {
        timer -= Time.deltaTime;
        if (controller.isGrounded)
        {
            if (timer < 0)
            {
                ResetAndRerandomize();
            }

            transform.eulerAngles += new Vector3(0, newAngle*Time.deltaTime*2f, 0);
            moveDirection = randomDir;
            moveDirection = transform.TransformDirection(moveDirection);
            moveDirection = moveDirection * speed;
        }

        moveDirection.y = moveDirection.y - (gravity * Time.deltaTime);
        controller.Move(moveDirection * Time.deltaTime);
    }
    

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "player")
        {
            randomDir = (transform.position - other.transform.position).normalized;
            newAngle = RandomizeAngle();
            timer = 2f;
            transform.rotation = new Quaternion(0, 0, 0, 0);
            StartCoroutine(Buff());
        }
        if(other.tag == "grid")
        {
            StartCoroutine(Debuff());
            ResetAndRerandomize();
        }
        if (other.tag == "pinguin")
        {
            ResetAndRerandomize();
        }
    }

    private void OnTriggerStay(Collider other)
    {     
        if(other.tag == "player")
        {                     
            randomDir = (transform.position - other.transform.position).normalized;
            transform.rotation = new Quaternion(0, 0, 0, 0);
            timer = 2f;
            StopAllCoroutines();
            StartCoroutine(Buff());
        }   
       
    }



    private Vector3 RandomizeDir()
    {
        return new Vector3(Random.Range(-1f, 1f), 0, Random.Range(-1f, 1f));
    }

    private float RandomizeAngle()
    {
        return Random.RandomRange(-80f, 80f);
    }

    private IEnumerator Buff()
    {
        speed = 8.5f;
        yield return new WaitForSecondsRealtime(1f);
        speed -= 2f;
    }

    private IEnumerator Debuff()
    {
        var oldSpeed = speed;
        speed = 0f;
        yield return new WaitForSecondsRealtime(0.8f);
        speed = oldSpeed;
       
    }

    private void ResetAndRerandomize()
    {
        timer = 2f;
        randomDir = RandomizeDir();
        newAngle = RandomizeAngle();
        transform.rotation = new Quaternion(0, 0, 0, 0);
    }
}
